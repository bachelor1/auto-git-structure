#!/bin/sh

#Token for access to Gitlab-api
Gitlab_token=""

#User password for login@stud.ntnu.no
ntnu_password=""

#Name of subject, effectively becomes root group in Gitlab course hierarchy
subjectCode="DCSG1005"

#Username for login@stud.ntnu.no
username=""

#Amount of tasks projects to be created for students 
amountTasks=1

#Name for output file containing all students
studentList="list_of_students.txt"

#Url for Gitlab API requests are sent to
baseURL="https://gitlab.stud.idi.ntnu.no/api/v4/"

#Name for the first section in URL for subject: for example https://...gitlab/nameOfSubject
path=$emnekode

#Retrieves current year, archive groups are named after the value retreived. For example 2022
groupArchiving=$(date +%Y)

#Name for the group to contain information for the public
groupPublic="Public-Information"

#Name for the group to contain information only available to designated users. 
groupPrivate="Private-Information"

#Name of the group to contain information only available to teachers and students
groupStudentOnly="Student-Only-Information"

#Name of group to contain all individual task projects for students
groupProjects="Projects"
