# find correct group. {{emnekode}}/projects/{{task}}
# find and save all projects in group
# demote all members in all projects to reporter (20)
# clone all projects (ssh_url_to_repo attribute whene list project)
# conferm clone
# Mabye promote members back to maintainer (40)


#!/bin/sh
source config.sh

# ask for name of task to be submitted

# find group owned
curl --location --request GET 'https://gitlab.stud.idi.ntnu.no/api/v4/groups?owned=true' --header 'Authorization: Bearer '${Gitlab_token}

# match all owned subgroup names to task, NB: year for archiving, task names may be the same

# find all project within group, group id must be found from previous curl
curl --location --request GET 'https://gitlab.stud.idi.ntnu.no/api/v4/groups/'${group_id}'/projects' --header 'Authorization: Bearer '${Gitlab_token}

# for-loop of all the projects 
for project in $projects

# list members of a project
curl --location --request GET 'https://gitlab.stud.idi.ntnu.no/api/v4/projects/16022/members' --header 'Authorization: Bearer '${Gitlab_token}

# for all members in project
for member in $project

# demot a member
curl --location --request PUT 'https://gitlab.stud.idi.ntnu.no/api/v4/projects/16022/members/'${member_id}'?access_level=20' --header 'Authorization: Bearer '${Gitlab_token}

# end for-loop of members in project

# clone repo
git clone ${ssh_url_to_repo}

# check if cloned
?

# for all members in project
for member in $project

# promote member
curl --location --request PUT 'https://gitlab.stud.idi.ntnu.no/api/v4/projects/16022/members/'${member_id}'?access_level=40' --header 'Authorization: Bearer '${Gitlab_token}

# end for-loop of members in project

# end for-loop of project in subgroup