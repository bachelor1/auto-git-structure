# Auto git structure

This is a gitlab project containing a script designed to automaticly create or update a gitlab structure of groups and projects. The gitlab structure is created with insperation from NTNU professor Mariusz's design. The script and gitlab structure is part of the result of bachelor project "git da gitt" spring 2022, and is design as an alternativ to established LMS(learning management system).

## How it works

The script will use the variables in config.sh to generate the following gitlab structure. 


1.    subjectCode
1.       -> currentYear
1.          -> Projects
1.             -> X amount of Tasks
1.                -> X amount of student projects in relation to task
1.          -> Student-Only-Information
1.          -> Private-Information
1.          -> Public-Information



The gitlab structure consits of groups and project with various access permitions. The root group is public and named your subject code. Under the root group (first tier of subgroups) are groups named after the year they where generated, this is for archiving purpose.

## Getting started

The script is designed to be run by a NTNU affiliated user, and as of current itterations list of students is retrieved with login.stud.ntnu.no and is only usable for students. The deploy.sh script can be edited to remove the LDAP search as long as the list of students is provided beforehand instead.

1. Create a gitlab token. If you dont have a gitlab user you need to create one. Navigate to your profile and "edit profile", here you can create a gitlab token. The token is needed to use gitlab's api and generate groups and projects.

2. Download the repository. It does not matter to where as you are only goning to run the script.

3. Dependencies:
apt-get install sshpass

4. Edit config.sh to match your data. Insert your gitlab token, change to subject code to match yours and so on.

5. Run the script on an NTNU instance. For instance through this command `ssh YOUR-NTNU-USERNAME@login.stud.ntnu.no`
