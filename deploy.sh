#!/bin/sh

######################################################
# Creates a subject group/project hierarchy in Gitlab
# Script is barebones and only meant to serve as an 
# example. Furthermore the script currently uses
# login.stud.ntnu.no LDAP server to retrieve a list of
# students from the course defined in ./config.sh
# If the user does not have an account on endpoint
# create the project hierarchy manually as depicted
# in bachelor report. If the list of students, name
# line by line, can be retrieved otherwise simply 
# remove the code following "Retrieve list of students
# from NTNU LDAP" comment below.
#
# Author: Nicholas Sellevåg
######################################################

. ./config.sh   #Retrieve the configured values in "config.sh" as environment variables

#Retrieve list of students from NTNU LDAP
sshpass -p $ntnu_password ssh $username@login.stud.ntnu.no 'ldapsearch -Z -h at.ntnu.no -D "" -b "ou=groups,dc=ntnu,dc=no" "cn=fs_$subjectCode"' | grep memberUid | awk '{print $2}' > $studentList

#Retrieve all groups user has from gitlab and store their names and id in groups.txt 
curl --location --request GET ''${baseURL}'groups?owned=true' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.[] | "\(.name) \(.id)"' | tr -d '"' > groups.txt

#Read each line in groups and check if the subject code exists already
groups="groups.txt"
while IFS= read -r line;
do
    i=$(echo $line | awk '{print $1}')
    echo "comparing: ${i} : ${emnekode}"
    #Check if the subject code matches retrieved subject groups from gitlab 
    if [ $i = $subjectCode ] 
    then
	#The subject group exists, store its ID
        root_gruppe_id=$(echo $line | awk '{print $2}')
    fi
done < "$groups" 

#Check if the subject group existed already
if [ -z "$root_gruppe_id" ]; 
then
    #The subject did not exist yet
    echo "Root group does not exist yet ${root_gruppe_id}"
    #Create the subject
    parent_id=$(curl --location --request POST ''${baseURL}'groups?name='${subjectCode}'&path='${path}'&visibility=public' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.id')
    
    #Create subgroup for archiving
    archiving_id=$(curl --location --request POST ''${baseURL}'groups?name='${groupArchiving}'&parent_id='${parent_id}'&path='${groupArchiving}'&visibility=public' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.id' )

    #Create subgroup for Public Info
    public_id=$(curl --location --request POST ''${baseURL}'groups?name='${groupPublic}'&parent_id='${archiving_id}'&path='${groupPublic}'&visibility=public' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.id' )

    #Create subgroup for Private Info
    private_id=$(curl --location --request POST ''${baseURL}'groups?name='${groupPrivate}'&parent_id='${archiving_id}'&path='${groupPrivate}'&visibility=private' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.id' )
    
    #Create subgroup for student only information
    studentOnly_id=$(curl --location --request POST ''${baseURL}'groups?name='${groupStudentOnly}'&parent_id='${archiving_id}'&path='${groupStudentOnly}'&visibility=private' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.id' )

    #Create subgroup for Projects
    projects_id=$(curl --location --request POST ''${baseURL}'groups?name='${groupProjects}'&parent_id='${archiving_id}'&path='${groupProjects}'&visibility=private' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.id' )

    #Create X amount of task groups
    i=0
    max=$amountTasks
    while [ $i -lt $max ]
    do
    task_id=$(curl --location --request POST ''${baseURL}'groups?name='Task${i}'&parent_id='${projects_id}'&path='Task${i}'&visibility=private' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.id')

    foo=""
      while IFS= read -r student;
      do
      #Create project for student within the task group
      studentProject_id=$(curl --location --request POST ''${baseURL}'projects?name='${student}'&visibility=private&namespace_id='${task_id}'' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.id')
      #Retrieve the student's ID
      student_id=$(curl --location --request GET ''${baseURL}'users?username='${student}'' --header 'Authorization: Bearer '${Gitlab_token}'' | jq '.[].id')
      #Add student to the task project created for them
      curl --location --request POST ''${baseURL}'projects/'${studentProject_id}'/members?user_id='${student_id}'&access_level=40' --header 'Authorization: Bearer '${Gitlab_token}''
      if [ "$foo" = "" ]
      then
	  foo="$student_id"
      else
      foo="${foo},$student_id"
      fi
      done < "$studentList"
    
    true $((i=i+1))
    done

    #Add student to internal group
    curl --location --request POST ''${baseURL}'groups/'${studentOnly_id}'/members?user_id='${foo}'&access_level=40' --header 'Authorization: Bearer '${Gitlab_token}''
else
    #The subject does exist already
    echo "Subject group already exist ${root_gruppe_id}"
    #TODO, verify that the created subject hierarchy was done successfully and matches the following structure:
    #SubjectName
    #  -> currentYear
    #       -> Projects
    #                -> x Amount of tasks
    #				       -> x task projects for each student
    #       -> Private-Information
    #       -> Public-Information
    #       -> Student-Only-Information
    #Dersom ikke opprett de som mangler/eventuelt endre på de som finnes dersom feil konfigurert, for eks med feil visibility?
fi
